FROM python:3.11.9 as prod

WORKDIR /app

COPY requirements.txt .
COPY SleepHarmonizer SleepHarmonizer
COPY project*.yaml ./

RUN pip install --upgrade pip setuptools wheel && \
    pip install --user -r requirements.txt

ENTRYPOINT ["python", "-m", "phases"]


FROM prod as prod-test

COPY tests tests
COPY requirements.dev.txt requirements.dev.txt

RUN  pip install --user -r requirements.dev.txt